﻿using Rss.Repository.Boundary.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rss.Repository.Boundary
{
    public interface IAppDbContext
    {
        DbSet<ChannelEntity> Channels { get; set; }
        DbSet<ItemEntity> Items { get; set; }
        
        void SaveChanges();
        void Create();
    }
}
