﻿using Rss.Repository.Boundary;
using Rss.Repository.Boundary.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Rss.Repository
{
    public class AppDbContext : DbContext, IAppDbContext
    {
        public AppDbContext() : base("Server=tcp:rssgetter.database.windows.net,1433;Initial Catalog=rss;Persist Security Info=False;User ID=eldo;Password=Zemczakchuj666;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=False;Connection Timeout=180;")
        {
            Configuration.LazyLoadingEnabled = false;
        }
        public DbSet<ChannelEntity> Channels { get; set; }
        public DbSet<ItemEntity> Items { get; set; }
        
        public void Create()
        {
            this.Database.Delete();
            this.Database.Create();
        }
        
        void IAppDbContext.SaveChanges()
        {
            this.SaveChanges();
        }
    }
}
