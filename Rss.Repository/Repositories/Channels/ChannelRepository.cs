﻿using System;
using System.Collections.Generic;
using System.Linq;
using Rss.Repository.Boundary;
using Rss.Repository.Boundary.Model;

namespace Rss.Repository.Repositories.Channels
{
    public class ChannelRepository : IChannelRepository
    {
        private readonly IAppDbContext _context;

        public ChannelRepository()
        {
            _context = new AppDbContext();
        }

        public void Usun()
        {
            var range = _context.Items.Where(n => n.PubDate.Contains("11 May"));
            _context.Items.RemoveRange(range);
            _context.SaveChanges();
        }

        public void AddItemsToChannel(ChannelEntity channel)
        {
            using (AppDbContext mycont = new AppDbContext())
            {
                var items = channel.Items.ToList();
                var inDb = mycont.Channels.Include("Items").First(n => n.XmlLink == channel.XmlLink);
                var itemsToAdd = new List<ItemEntity>();

                foreach (var item in items)
                {
                    var contains = inDb.Items.Any(itemInDb => item.Link == itemInDb.Link);
                    if (!contains)
                        itemsToAdd.Add(item);
                }
                
                if (itemsToAdd.Count() != 0)
                {
                    var itemsToAddNoDupes = itemsToAdd.GroupBy(n => n.Title)
                        .Select(group => group.First()).ToList();
                    foreach (var item in itemsToAddNoDupes)
                    {
                        inDb.Items.Add(item);
                    }
                }
                inDb.LastBuildDate = channel.LastBuildDate;
                mycont.SaveChanges();
            }
        }

        public IEnumerable<ChannelEntity> GetChannels()
        {
            return _context.Channels;
        }

        public ChannelEntity GetChannelByLink(string link)
        {
            return _context.Channels.FirstOrDefault(n => n.XmlLink == link);
        }

        public void CreateDb()
        {
            _context.Create();
        }

        public void AddChannel(ChannelEntity channel)
        {
            _context.Channels.Add(channel);
            _context.SaveChanges();
        }
        
        public ChannelEntity GetChannelById(int id)
        {
            return _context.Channels.FirstOrDefault(n => n.Id == id);
        }
        
        public Lazy<IEnumerable<ItemEntity>> GetFreshNews(int channelId, int page)
        {
            return new Lazy<IEnumerable<ItemEntity>>(() => GetNewsWithFilter(channelId,string.Empty, page));
        }

        public Lazy<IEnumerable<ItemEntity>> GetFilteredNews(int channelId, string filter, int page)
        {
            return new Lazy<IEnumerable<ItemEntity>>(() => GetNewsWithFilter(channelId, filter, page));
        }

        private IEnumerable<ItemEntity> GetNewsWithFilter(int channelId, string filter, int page)
        {
            return string.IsNullOrWhiteSpace(filter) ? _context.Items.Where(n => n.ChannelId == channelId).OrderByDescending(n => n.Id).Skip(6 * page).Take(6) : _context.Items.Where(n => n.ChannelId == channelId && (n.Description.ToLower().Contains(filter.ToLower()) || n.Title.ToLower().Contains(filter.ToLower()))).OrderByDescending(n => n.Id).Skip(6 * page).Take(6);
        }

        public void ClearDoubledInChannel(int id)
        {
            var itemsToDist = _context.Items.Where(n => n.ChannelId == id).ToList();
            var itemsCleared = itemsToDist.GroupBy(n => n.Link).Select(group => group.First()).ToList();
            _context.Items.RemoveRange(itemsToDist);
            _context.SaveChanges();
            _context.Items.AddRange(itemsCleared);
            _context.SaveChanges();
        }

        public int ChannelsCount()
        {
            return _context.Channels.Count();
        }

        public int GetItemsCount(int channelId)
        {
            return _context.Items.Count(n => n.ChannelId == channelId);
        }
    }
}
