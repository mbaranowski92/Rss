﻿using Rss.Repository.Boundary;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rss.Repository.Boundary.Model;

namespace Rss.Repository.Repositories.Fake
{
    public class FakeChannelRepository : IChannelRepository
    {
        List<ChannelEntity> _fakeChannels;
        List<ItemEntity> _fakeItems;

        public FakeChannelRepository()
        {
            _fakeChannels = new List<ChannelEntity>();
            _fakeItems = new List<ItemEntity>();

            _fakeItems.Add(new ItemEntity { Description="test", ChannelId=1, Id = 1, Title = "item title", Link = "link item", PubDate = DateTime.Now.ToString() });
            _fakeChannels.Add(new ChannelEntity { Id = 1, Description = "test", Items = new List<ItemEntity> { new ItemEntity { Id = 1, Title = "item title", Link = "link item", PubDate = DateTime.Now.ToString() } }, Language = "pl", LastBuildDate = DateTime.Now.ToString(), Link = "link", Title = "test title", XmlLink = "xml link" });
        }

        public void AddChannel(ChannelEntity channel)
        {
            _fakeChannels.Add(channel);
        }

        public void Usun()
        {
            throw new NotImplementedException();
        }

        public void AddItemsToChannel(ChannelEntity channel)
        {
            ///???
        }

        public int ChannelsCount()
        {
            return _fakeChannels.Count();
        }

        public Lazy<IEnumerable<ItemEntity>> GetFilteredNews(int id, string filter, int skip)
        {
            throw new NotImplementedException();
        }

        public void ClearDoubledInChannel(int id)
        {
            ///??????????
        }

        public Lazy<IEnumerable<ItemEntity>> GetFreshNews(int channelId, int skip)
        {
            throw new NotImplementedException();
        }

        public void CreateDb()
        {
            
        }

        public ChannelEntity GetChannelById(int id)
        {
            return _fakeChannels.FirstOrDefault(n => n.Id == id);
        }

        public ChannelEntity GetChannelByLink(string link)
        {
            return _fakeChannels.FirstOrDefault(n => n.Link == link);
        }

        public IEnumerable<ChannelEntity> GetChannels()
        {
            return _fakeChannels;
        }

        public Lazy<IEnumerable<ItemEntity>> GetFilteredNews(int id, string filter)
        {
            var result = _fakeItems.Where(n => n.ChannelId == id && (n.Description.ToLower().Contains(filter.ToLower()) || n.Title.ToLower().Contains(filter.ToLower())));
            return result as Lazy<IEnumerable<ItemEntity>>;
        }

        public Lazy<IEnumerable<ItemEntity>> GetFreshNews(int channelId)
        {
            var result = _fakeItems.OrderByDescending(n => n.Id).Where(n => n.ChannelId == channelId);
            return result as Lazy<IEnumerable<ItemEntity>>;
        }

        public int GetItemsCount(int channelId)
        {
            return _fakeItems.Count(n => n.ChannelId == channelId);
        }
    }
}
