﻿using NSubstitute;
using NUnit.Framework;
using Rss.Controllers;
using Rss.Domain.Logic;
using Rss.Factory;
using Rss.Repository.Boundary.Model;
using Rss.Repository.Repositories.Fake;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Rss.Tests.ControllersTests
{
    public class ChannelControllerTests
    {
        private ChannelController _controller;
        private ObjectFactory _objectFactory;

        public ChannelControllerTests()
        {
            _objectFactory = Substitute.For<ObjectFactory>(new FakeChannelRepository());
            _controller = new ChannelController(_objectFactory);
        }

        [Test]
        public void News_ReturnsView()
        {
            var result = _controller.News(1,1) as ViewResult;
            Assert.NotNull(result);
        }

        [Test]
        public void GetChannel_ReturnsChannelsFromFake()
        {
            var jsonResult = _controller.GetChannel(1, 1);
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            ChannelEntity result = serializer.Deserialize<ChannelEntity>(serializer.Serialize(jsonResult.Data));
            var converted = _objectFactory.GetViewModelConverter().Convert(result);

            Assert.True(converted.Description.Contains("test"));
            Assert.True(converted.Items.FirstOrDefault().Description.Contains("test"));
        }
        
        [Test]
        public void GetChannelWithFilter_ShouldReturnFilteredChannels()
        {
            var jsonResult = _controller.GetChannelWithFilter(1, "test");
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            ChannelViewModel result = serializer.Deserialize<ChannelViewModel>(serializer.Serialize(jsonResult.Data));

            Assert.True(result.Items.FirstOrDefault().Description.Contains("test"));
        }
    }
}
