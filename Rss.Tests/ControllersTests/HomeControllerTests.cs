﻿using NSubstitute;
using NUnit.Framework;
using Rss.Controllers;
using Rss.Domain.Logic;
using Rss.Factory;
using Rss.Repository.Repositories.Fake;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Rss.Domain.Services;

namespace Rss.Tests.ControllersTests
{
    public class HomeControllerTests
    {
        private readonly HomeController _controller;
        private readonly ObjectFactory _objectFactory;
        private DbService _service;

        public HomeControllerTests()
        {
            _service = Substitute.For<DbService>();
            _objectFactory = Substitute.For<ObjectFactory>();
            _controller = new HomeController(_objectFactory);
        }

        [Test]
        public void Index_ReturnsView()
        {
            var result = _controller.Index() as ViewResult;
            Assert.NotNull(result);
        }

        [Test]
        public void PobierzLinki_ReturnsLinks()
        {
            var jsonResult = _controller.PobierzLinki();
            var serializer = new JavaScriptSerializer();
            var result = serializer.Deserialize<List<string>>(serializer.Serialize(jsonResult.Data));

            Assert.True(result.All(n => n.Contains("http://")));
        }

        [Test]
        public void PobierzKanaly_ReturnsChannels()
        {
            var jsonResult = _controller.PobierzKanaly();
            var serializer = new JavaScriptSerializer();
            var result = serializer.Deserialize<List<ChannelViewModel>>(serializer.Serialize(jsonResult.Data));

            Assert.True(result.FirstOrDefault().Description.Contains("test"));
        }
    }
}
