﻿using Rss.Repository.Boundary.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rss.Repository.Boundary
{
    public interface IChannelRepository
    {
        void Usun();
        void AddItemsToChannel(ChannelEntity channel);
        IEnumerable<ChannelEntity> GetChannels();
        void AddChannel(ChannelEntity channel);
        ChannelEntity GetChannelByLink(string link);
        ChannelEntity GetChannelById(int id);
        Lazy<IEnumerable<ItemEntity>> GetFreshNews(int channelId, int skip);
        void CreateDb();
        Lazy<IEnumerable<ItemEntity>> GetFilteredNews(int id,string filter, int skip);
        void ClearDoubledInChannel(int id);
        int GetItemsCount(int channelId);
        int ChannelsCount();
    }
}
