﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rss.Repository.Boundary.Model
{
    [Table("Items")]
    public class ItemEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public string PubDate { get; set; }
        public string Guid { get; set; }

        public int ChannelId { get; set; }

        public virtual ChannelEntity Channel { get; set; }
    }
}
