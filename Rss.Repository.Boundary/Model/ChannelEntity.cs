﻿using Rss.Repository.Boundary.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rss.Repository.Boundary.Model
{
    [Table("Channels")]
    public class ChannelEntity
    {
        public ChannelEntity()
        {
            Items = new List<ItemEntity>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string XmlLink { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public string LastBuildDate { get; set; }

        public virtual List<ItemEntity> Items { get; set; }
    }
}
