﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rss.Domain.Logic;

namespace Rss.Domain.Boundary.Services
{
    public interface IDbService
    {
        void Usun();
        void AddItemsToChannel(ChannelViewModel channel);
        IEnumerable<ChannelViewModel> GetChannels();
        IEnumerable<ChannelViewModel> GetLastViewedChannels(List<string> lastViewedIds);
        void AddChannel(ChannelViewModel channel);
        ChannelViewModel GetChannelByLink(string link);
        ChannelViewModel GetChannelById(int id);
        IEnumerable<ItemViewModel> GetFreshNews(int channelId,string filter,int page);
        void CreateDb();
        IEnumerable<ItemViewModel> GetFilteredNews(int id, string filter, int page);
        void ClearDoubledInChannel(int id);
        int GetItemsCount(int channelId);
        int ChannelsCount();
        IEnumerable<ItemViewModel> GetTopNewsOf(int mostViewedCategory);
    }
}
