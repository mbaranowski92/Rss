﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace Rss.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angularjs").Include(
            "~/Scripts/angular.js",
            "~/Scripts/angular-sanitize.js",
            "~/Scripts/ng-infinite-scroll.min.js",
            "~/Scripts/angular-animate.js"));


            bundles.Add(new ScriptBundle("~/bundles/modules").Include(
            "~/Scripts/app.js"));
            

            bundles.Add(new ScriptBundle("~/bundles/pagination").Include(
            "~/Scripts/dirPagination.js"));

            bundles.Add(new ScriptBundle("~/bundles/controllers")
                .IncludeDirectory("~/Scripts/controllers", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/services")
                .IncludeDirectory("~/Scripts/services", "*.js", true));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/news.css"));

        }
    }
}