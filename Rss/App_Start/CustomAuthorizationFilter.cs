﻿using System;
using Hangfire.Annotations;
using Hangfire.Dashboard;

namespace Rss.App_Start
{
    internal class CustomAuthorizationFilter:IDashboardAuthorizationFilter
    {
        public CustomAuthorizationFilter()
        {
        }

        public bool Authorize([NotNull] DashboardContext context)
        {
            return true;
        }
    }
}