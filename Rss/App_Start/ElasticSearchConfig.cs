﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nest;
using Rss.Repository;
using Rss.Repository.Boundary.Model;

namespace Rss.App_Start
{
    public static class ElasticSearchConfig
    {
        public static void ConfigChannels()
        {
            var uri = new Uri("http://localhost");
            var builder = new UriBuilder(uri);
            builder.Port = 9200;

            var settings = new ConnectionSettings(builder.Uri);
            settings.DisableDirectStreaming();
            settings.DefaultIndex("channels");

            var client = new ElasticClient(settings);
            using (var context = new AppDbContext())
            {
                foreach (var channel in context.Channels)
                {
                    client.Index(channel);
                }
            }
        }

        public static void ConfigItems()
        {
            var uri = new Uri("http://localhost");
            var builder = new UriBuilder(uri);
            builder.Port = 9200;

            var setting = new ConnectionSettings(builder.Uri);
            setting.DisableDirectStreaming();
            setting.DefaultIndex("items");
            var client = new ElasticClient(setting);

            using (var context = new AppDbContext())
            {
                foreach (var item in context.Items)
                {
                    client.Index(item);
                }
            }
        }
    }
}