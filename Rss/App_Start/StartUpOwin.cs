﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Hangfire;
using System.Web.Mvc;
using Rss.Repository.Boundary;
using Rss.Repository;
using Autofac;
using Autofac.Integration.Mvc;
using Rss.Factory;
using System.Reflection;
using System.Web.Routing;
using System.Web.Optimization;
using System.Web.UI.WebControls;
using Rss.Domain.Boundary.Services;
using Rss.Domain.Logic.Helpers;
using Rss.Domain.Services;
using Rss.Repository.Repositories.Channels;

[assembly: OwinStartup(typeof(Rss.App_Start.StartUpOwin))]

namespace Rss.App_Start
{
    public class StartUpOwin
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureDependencies();
            ConfigureRoutes();
            ConfigureHangdash(app);
            ConfigureHangdashJobs();
            ConfigureElasticSearch();
        }

        private void ConfigureElasticSearch()
        {
            //ElasticSearchConfig.ConfigChannels();
            //ElasticSearchConfig.ConfigItems();
        }

        private void ConfigureRoutes()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void ConfigureHangdashJobs()
        {
            RecurringJob.AddOrUpdate<DbUpdater>(
                            dbUpdater => dbUpdater.UpdateDatabase(),
                            Cron.Hourly);
        }

        private void ConfigureHangdash(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("Server=tcp:rssgetter.database.windows.net,1433;Initial Catalog=rss;Persist Security Info=False;User ID=eldo;Password=Zemczakchuj666;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=180;");
            var options = new DashboardOptions
            {
                Authorization = new[] { new CustomAuthorizationFilter() }
            };
            app.UseHangfireDashboard("/hangdash", options);
            app.UseHangfireServer();
        }

        private void ConfigureDependencies()
        {
            ContainerBuilder builder = RegisterTypes();
            var container = builder.Build();
            GlobalConfiguration.Configuration.UseAutofacActivator(container);
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }

        private ContainerBuilder RegisterTypes()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<AppDbContext>().AsImplementedInterfaces();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<ChannelRepository>().As<IChannelRepository>();
            builder.RegisterType<ViewModelConverter>().As<IViewModelConverter>();
            builder.RegisterType<DbService>().As<IDbService>();
            builder.RegisterType<ObjectFactory>().As<IObjectFactory>();
            builder.RegisterType<DbUpdater>().InstancePerBackgroundJob();
            return builder;
        }
    }
}
