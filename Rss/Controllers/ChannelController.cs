﻿using Rss.Factory;
using Rss.Repository.Boundary.Model;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Nest;
using Rss.Domain.Logic;

namespace Rss.Controllers
{
    public class ChannelController : Controller
    {
        private readonly IObjectFactory _factory;

        public ChannelController(IObjectFactory factory)
        {
            _factory = factory;
        }
        public ActionResult News(int id)
        {
            return View();
        }

        public JsonResult FindCategory(string myquery)
        {
            var uri = new Uri("http://localhost");
            var builder = new UriBuilder(uri);
            builder.Port = 9200;

            var setting = new ConnectionSettings(builder.Uri);
            setting.DefaultIndex("items");
            setting.DisableDirectStreaming();
            var client = new ElasticClient(setting);

            var result = client.Search<ChannelEntity>(body =>
                body.Query(query =>
                        query.ConstantScore(
                            csq => csq.Filter(filter =>
                                filter.Term(x =>
                                    x.Title, myquery))))
                    .Take(1000));

            //var result = client.Search<ChannelEntity>(body => body.Query(query=>query.QueryString(qs=>qs.Query(myquery))));
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [Route("/GetChannel/{id}/{filter}/{page}")]
        public JsonResult GetChannel(int id, string filter, int page)
        {
            var channelToShow = _factory.GetDbService().GetChannelById(id);
            channelToShow.Items = _factory.GetDbService().GetFreshNews(id,filter,page).OrderBy(n =>n.PubDate).ToList();
            
            return Json(channelToShow, JsonRequestBehavior.AllowGet);
        }

        public void Usun()
        {
            _factory.GetDbService().Usun();
        }

        [Route("/GetChannelWithFilter/{id}/{filter}/{page}")]
        public JsonResult GetChannelWithFilter(int id, string filter, int page)
        {
            var channelToShow = _factory.GetDbService().GetChannelById(id);
            channelToShow.Items = _factory.GetDbService().GetFilteredNews(id, filter, page).OrderBy(n=>n.PubDate).ToList();

            return Json(_factory.GetViewModelConverter().Convert(channelToShow), JsonRequestBehavior.AllowGet);
        }
    }
}