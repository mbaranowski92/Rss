﻿using System;
using System.Linq;
using System.Web.Mvc;
using Rss.Domain.Logic;
using Rss.Factory;
using Rss.Domain.Logic.Helpers;

namespace Rss.Controllers
{
    public class HomeController : Controller
    {
        private readonly IObjectFactory _factory;

        public HomeController(IObjectFactory factory)
        {
            _factory = factory;
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [Route("PobierzLinki")]
        public JsonResult PobierzLinki()
        {
            return Json(XmlLinkGetter.GetLinksFromUrl(), JsonRequestBehavior.AllowGet);
        }

        [Route("StworzBaze")]
        public void StworzBaze()
        {
            _factory.GetDbUpdater().CreateNewDb();
        }

        [Route("PobierzKanaly")]
        public JsonResult PobierzKanaly()
        {
            var channels = _factory.GetDbService().GetChannels().ToList();
            return Json(channels, JsonRequestBehavior.AllowGet);
        }


        public void SaveChoiceToCookies(string channelId)
        {
            _factory.GetCookieManager().SetCookie("mostViewedCategories",channelId, TimeSpan.FromDays(7));
        }

        public JsonResult GetTopNews()
        {
            var mostViewedCategory = _factory.GetCookieManager().GetCookie("mostViewedCategories");
            return Json(_factory.GetDbService().GetTopNewsOf(mostViewedCategory), JsonRequestBehavior.AllowGet);
        }

        [Route("KasujPodwojne/{id}")]
        public void KasujPodwojne(int id)
        {
            if (id == 0)
            {
                Hangfire.BackgroundJob.Enqueue<DbUpdater>(
                    n => n.ClearDoubledInDb()
                    );
            }
            else
                Hangfire.BackgroundJob.Enqueue<DbUpdater>(
                    n => n.ClearDoubledInChannel(id)
                    );
        }
    }
}