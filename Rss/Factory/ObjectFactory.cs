﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Rss.Domain.Boundary.Services;
using Rss.Domain.Logic.Helpers;
using Rss.Repository.Boundary;

namespace Rss.Factory
{
    public class ObjectFactory : IObjectFactory
    {
        private readonly IDbService _service;
        private readonly IChannelRepository _repo;

        public ObjectFactory(IDbService service, IChannelRepository repo)
        {
            _service = service;
            _repo = repo;
        }

        public IDbService GetDbService()
        {
            return _service;
        }

        public CookieManager GetCookieManager()
        {
            return new CookieManager();
        }

        public DbUpdater GetDbUpdater()
        {
            return new DbUpdater(_repo);
        }

        public ViewModelConverter GetViewModelConverter()
        {
            return new ViewModelConverter();
        }
    }
}