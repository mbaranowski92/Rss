﻿using Rss.Domain.Boundary.Services;
using Rss.Domain.Logic.Helpers;

namespace Rss.Factory
{
    public interface IObjectFactory
    {
        IDbService GetDbService();
        CookieManager GetCookieManager();
        DbUpdater GetDbUpdater();
        ViewModelConverter GetViewModelConverter();
    }
}