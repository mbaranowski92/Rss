﻿app.controller("HomeCtrl", ["$scope","$http","dbCommunicationService", function ($scope, $http, dbCommunicationService) {

    $scope.commands = {};
    $scope.channels = [];
    $scope.loader = true;

    $scope.selectedCategory = "";
    $scope.categories = [];
    $scope.top = {};

    var getTopNews = function () {
        $http.get('/Home/GetTopNews').then(function (result) {
            $scope.top.Items = result.data;
            console.log($scope.top.Items);
        });
    };

    $scope.commands.OpenChannel = function(id) {
        $http.get('/Home/SaveChoiceToCookies/?channelId=' + id).then(function() {
            window.location.href = "/Channel/News/" + id;
        });
    };

    dbCommunicationService.getChannels().then(function (data) {
        $scope.loader = false;
        $scope.channels = data;
        $scope.categories = data.map(function(a) {return a.Title;});
    });

    $scope.getImage = function (img) {

        if (typeof img !== "undefined") {
            var url = img;

            var check_start = url.indexOf("<img");
            var check_end = url.indexOf("src");
            var check = url.substring(check_start, check_end - 1)

            var end_index = url.lastIndexOf("type=1");
            var start_index = url.indexOf("http");
            var _result = url.substring(start_index, end_index)

            var result = _result + "type=1&srcmode=0&srcx=1/1&srcy=1/1&srcw=1/1&srch=1/1&dstw=640&dsth=360&quality=80";
            //var tvn24 = "http://i.imgur.com/E64zxIQ.png";
            var tvn24 = "http://www.404notfound.fr/assets/images/pages/img/lego.jpg";

            if (check != "<img") {
                return tvn24;
            } else {
                return result;
            }
        }
    }


    $scope.shortDescription = function (desc) {

        if (typeof desc !== 'undefined') {
            var cut_start = desc.indexOf("/>")
            var cut_desc = desc.substring(cut_start + 2, desc.length)

            var check_start = desc.indexOf("<img");
            var check_end = desc.indexOf("src");
            var check = desc.substring(check_start, check_end - 1)

            if (check != '<img') {
                return desc;
            } else {
                return cut_desc;
            }
        }
    }

    $scope.changeActiveNews = function (index) {
        $scope.activeNews = index;
    }

    getTopNews();

}]);


app.directive('scrollOnClick', function () {
    return {
        restrict: 'A',
        link: function (scope, $elm, attrs) {
            var idToScroll = attrs.href;
            $elm.on('click', function () {
                var $target;
                if (idToScroll) {
                    $target = $(idToScroll);
                } else {
                    $target = $elm;
                }
                $("body").animate({ scrollTop: $target.offset().top }, "slow");
            });
        }
    }
});