﻿app.controller('ChannelCtrl', ['$scope', function ($scope) {

    $scope.data = {};
    $scope.data.searchText = "";
    $scope.activeNews = {};
    $scope.data.hidenews = false;

    var page = 0;

    $scope.data.loader = false;

    var getIdFromUrl = function () {
        var url = window.location.href;
        var index = url.lastIndexOf("/");
        var result = url.substring(index + 1, index + 3);

        return result;
    }



    $scope.changeActiveNews = function (index) {
        $scope.activeNews = index;
    }

    $scope.getImage = function (img) {

        if (typeof img !== "undefined") {
            var url = img;

            var check_start = url.indexOf("<img");
            var check_end = url.indexOf("src");
            var check = url.substring(check_start, check_end - 1);

            var end_index = url.lastIndexOf("type=1");
            var start_index = url.indexOf("http");
            var _result = url.substring(start_index, end_index)

            var result = _result + "type=1&srcmode=0&srcx=1/1&srcy=1/1&srcw=1/1&srch=1/1&dstw=640&dsth=360&quality=80";
            //var tvn24 = "http://i.imgur.com/E64zxIQ.png";
            var tvn24 = "http://www.404notfound.fr/assets/images/pages/img/lego.jpg";

            if (check != "<img") {
                return tvn24;
            } else {
                return result;
            }
        }
    }


    $scope.shortDescription = function (desc) {

        if (typeof desc !== 'undefined') {
            var cut_start = desc.indexOf("/>")
            var cut_desc = desc.substring(cut_start + 2, desc.length)

            var check_start = desc.indexOf("<img");
            var check_end = desc.indexOf("src");
            var check = desc.substring(check_start, check_end - 1)

            if (check != '<img') {
                return desc;
            } else {
                return cut_desc;
            }
        }
    }

    var getChannel = function () {

        if ($scope.data.loader !== true) {
            $scope.data.loader = true;

            if (page === 0)
                $scope.data.hidenews = true;

            $.ajax({
                url: "/Channel/GetChannel/" + getIdFromUrl() + "?filter=" + $scope.data.searchText + "&page=" + page,
                type: "GET",

                success: function(data) {
                    if ($scope.data.channel === undefined)
                        $scope.data.channel = data;
                    else
                        $scope.data.channel.Items = $scope.data.channel.Items.concat(data.Items);

                    $scope.data.loader = false;
                    $scope.data.hidenews = false;
                    page = page + 1;
                    
                    $scope.$apply();
                }
            });
        }
    };

    $scope.GetChannel = function () {
        getChannel();
    };

    $scope.GetFilteredNews = function () {
        page = 0;

        $scope.data.loader = true;
        $.ajax({
            url: "/Channel/GetChannel/" + getIdFromUrl() + "?filter=" + $scope.data.searchText + "&page=" + page,
            type: "GET",

            success: function (data) {

                $scope.data.channel = data;

                $scope.data.loader = false;
                page = page + 1;
                $scope.$apply();
            }
        });
    };

    getChannel();
}]);


