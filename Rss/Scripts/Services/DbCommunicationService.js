﻿app.service('dbCommunicationService',[ '$http', function ($http) {
    return {
        getLinks: function () {
            return $http.get('/Home/PobierzLinki').then(function (result) {
                return result.data;
            });
        }
    }
}]);

app.service('dbCommunicationService',['$http', function ($http) {
    return {
        getChannels: function () {
            return $http.get('/Home/PobierzKanaly').then(function (result) {
                return result.data;
            });
        }
    }
}]);