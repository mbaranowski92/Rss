﻿using Autofac;
using Autofac.Integration.Mvc;
using Rss.App_Start;
using Rss.Factory;
using Rss.Repository;
using Rss.Repository.Boundary;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Rss
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
        }
    }
}
