﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Rss.Domain.Boundary.Services;
using Rss.Domain.Logic;
using Rss.Domain.Logic.Helpers;
using Rss.Repository.Boundary;
using Rss.Repository.Boundary.Model;

namespace Rss.Domain.Services
{
    public class DbService : IDbService
    {
        private readonly IChannelRepository _repository;
        private readonly IViewModelConverter _converter;

        public DbService(IChannelRepository repository, IViewModelConverter converter)
        {
            _repository = repository;
            _converter = converter;
        }


        public void Usun()
        {
            _repository.Usun();
        }

        public void AddItemsToChannel(ChannelViewModel channel)
        {
            _repository.AddItemsToChannel(_converter.Convert(channel));
        }

        public IEnumerable<ChannelViewModel> GetChannels()
        {
            var channels = _repository.GetChannels().Select(n => _converter.Convert(n));
            var result = channels.Select(n => new ChannelViewModel
            {
                ItemsCount = _repository.GetItemsCount(n.Id),
                Id = n.Id,
                Description = n.Description,
                Items = n.Items,
                Language = n.Language,
                LastBuildDate = n.LastBuildDate,
                Link = n.Link,
                Title = n.Title,
                XmlLink = n.XmlLink
            });

            return result;
        }

        public IEnumerable<ChannelViewModel> GetLastViewedChannels(List<string> lastViewedIds)
        {
            return GetChannels().Where(n => lastViewedIds.Contains(n.Id.ToString()));
        }

        public void AddChannel(ChannelViewModel channel)
        {
            _repository.AddChannel(_converter.Convert(channel));
        }

        public ChannelViewModel GetChannelByLink(string link)
        {
            return _converter.Convert(_repository.GetChannelByLink(link));
        }

        public ChannelViewModel GetChannelById(int id)
        {
            return _converter.Convert(_repository.GetChannelById(id));
        }

        public IEnumerable<ItemViewModel> GetFreshNews(int channelId,string filter,int page)
        {
            List<ItemEntity> items;
            if (string.IsNullOrEmpty(filter))
                items = _repository.GetFreshNews(channelId, page).Value.ToList();
            else
                items = _repository.GetFilteredNews(channelId, filter, page).Value.ToList();

            var result = _converter.ConvertItems(items);
            return result;
        }

        public void CreateDb()
        {
            _repository.CreateDb();
        }

        public IEnumerable<ItemViewModel> GetFilteredNews(int id, string filter,int page)
        {
            return _converter.ConvertItems(_repository.GetFilteredNews(id, filter,page).Value.ToList());
        }

        public void ClearDoubledInChannel(int id)
        {
            _repository.ClearDoubledInChannel(id);
        }

        public int GetItemsCount(int channelId)
        {
            return _repository.GetItemsCount(channelId);
        }

        public int ChannelsCount()
        {
            return _repository.ChannelsCount();
        }

        public IEnumerable<ItemViewModel> GetTopNewsOf(int mostViewedCategory)
        {
            var result = _repository.GetFreshNews(mostViewedCategory,0).Value
                .OrderBy(n => DateFormatter.FormatDate(n.PubDate)).Take(3).ToList();
            return _converter.ConvertItems(result);
        }
    }
}
