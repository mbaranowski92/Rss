﻿using System.Collections.Generic;

namespace Rss.Domain.Logic
{
    public class ChannelViewModel
    {
        public ChannelViewModel()
        {
            Items = new List<ItemViewModel>();
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string XmlLink { get; set; }
        public string Description { get; set; }
        public string Language { get; set; }
        public string LastBuildDate { get; set; }
        public int ItemsCount { get; set; }

        public List<ItemViewModel> Items { get; set; }
    }
}
