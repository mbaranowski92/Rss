﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using AutoMapper;
using Rss.Repository.Boundary.Model;

namespace Rss.Domain.Logic.Helpers
{
    public class ViewModelConverter : IViewModelConverter
    {
        public ChannelEntity Convert(ChannelViewModel entity)
        {
            var items = ConvertItems(entity.Items);
            Mapper.Initialize(cfg => cfg.CreateMap<ChannelViewModel, ChannelEntity>().ForMember(r => r.Items, opt => opt.Ignore()));
            var result = Mapper.Map<ChannelEntity>(entity);
            result.Items = items;
            return result;
        }

        public List<ItemEntity> ConvertItems(List<ItemViewModel> entities)
        {
            Mapper.Initialize(cfg => cfg.CreateMap<ItemViewModel, ItemEntity>());
            return entities.Select(Mapper.Map<ItemEntity>).ToList();
        }

        public List<ItemViewModel> ConvertItems(List<ItemEntity> entities)
        {
            var results = new List<ItemViewModel>();

            foreach (var item in entities)
            {
                DateTime date;

                if (!string.IsNullOrEmpty(item.PubDate))
                {
                    item.PubDate = item.PubDate.Remove(item.PubDate.Length - 6, 6);
                    date = DateTime.ParseExact(item.PubDate, "ddd, d MMM yy HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else
                    date = DateTime.MinValue;

                results.Add(new ItemViewModel {Description = item.Description,Guid = item.Guid,Id = item.Id,Link = item.Link,Title = item.Title, PubDate = date});
            }

            return results;
        }
        
        public ChannelViewModel Convert(ChannelEntity entity)
        {
            var items = ConvertItems(entity.Items);
            Mapper.Initialize(cfg => cfg.CreateMap<ChannelEntity, ChannelViewModel>().ForMember( r => r.Items, opt=> opt.Ignore()));
            var result = Mapper.Map<ChannelViewModel>(entity);
            result.Items = items;
            return result;
        }
    }
}