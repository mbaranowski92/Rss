﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rss.Domain.Logic.Helpers
{
    public class CookieManager
    {
        private string _startValue = "0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0";

        public void SetCookie(string key, string value, TimeSpan expires)
        {
            var myCookie = new HttpCookie(key, _startValue);

            if (HttpContext.Current.Request.Cookies[key] != null)
            {
                var cookieOld = HttpContext.Current.Request.Cookies[key];
                cookieOld.Expires = DateTime.Now.Add(expires);

                AddValueToCookie(value, cookieOld);

                HttpContext.Current.Response.Cookies.Add(cookieOld);
            }
            else
            {
                myCookie.Expires = DateTime.Now.Add(expires);
                AddValueToCookie(value, myCookie);
                HttpContext.Current.Response.Cookies.Add(myCookie);
            }
        }

        private static void AddValueToCookie(string value, HttpCookie cookie)
        {
            var list = StringListConverter.ToList(cookie.Value);
            var index = Convert.ToInt32(value) - 1;
            list[index] = (Convert.ToInt32(list[index])+1).ToString();

            var newValue = StringListConverter.ToString(list);
            cookie.Value = newValue;
        }

        public int GetCookie(string key)
        {
            var cookie = HttpContext.Current.Request.Cookies[key];

            if (string.IsNullOrEmpty(cookie?.Value))
                return 0;

            var list = StringListConverter.ToList(cookie.Value);
            var maxValue = list.Max();

            return list.IndexOf(list.FirstOrDefault(n => n == maxValue)) + 1;
        }

    }
}
