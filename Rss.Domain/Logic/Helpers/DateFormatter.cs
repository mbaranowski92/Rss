﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rss.Domain.Logic.Helpers
{
    public static class DateFormatter
    {
        public static DateTime FormatDate(string date)
        {
            if (string.IsNullOrEmpty(date))
                return DateTime.MinValue;

            var index = date.IndexOf("+");
            var result = date.Substring(0, index-1);

            return DateTime.ParseExact(
                result,
                "ddd, dd MMM yy HH:mm:ss",
                CultureInfo.InvariantCulture);
        }
    }
}
