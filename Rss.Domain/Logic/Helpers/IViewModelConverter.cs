﻿using System.Collections.Generic;
using Rss.Repository.Boundary.Model;

namespace Rss.Domain.Logic.Helpers
{
    public interface IViewModelConverter
    {
        ChannelViewModel Convert(ChannelEntity entity);
        ChannelEntity Convert(ChannelViewModel entity);

        List<ItemViewModel> ConvertItems(List<ItemEntity> entities);
        List<ItemEntity> ConvertItems(List<ItemViewModel> entities);
    }
}