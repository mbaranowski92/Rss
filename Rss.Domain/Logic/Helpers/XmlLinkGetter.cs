﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml;
using HtmlAgilityPack;

namespace Rss.Domain.Logic.Helpers
{
    public static class XmlLinkGetter
    {
        public static List<string> GetLinksFromUrl(string url = "http://www.tvn24.pl/rss.html")// returns all xml links from website
        {
            using (var client = new WebClient())
            {
                var htmlSource = client.DownloadString(url);
                return FindXmlLinks(GetLinksFromWebsite(htmlSource));
            }
        }

        private static List<String> GetLinksFromWebsite(string htmlSource)// returns list of links on website
        {
            var doc = new HtmlDocument();
            doc.LoadHtml(htmlSource);
            return doc
                .DocumentNode
                .SelectNodes("//a[@href]")
                .Select(node => node.Attributes["href"].Value)
                .ToList();
        }

        private static List<string> FindXmlLinks(List<string> listOfLinks) //Filters list to return only .xml links
        {
            List<string> newList = new List<string>();
            foreach(string link in listOfLinks)
            {
                if (link.EndsWith(".xml") && LinkWorks(link)) newList.Add(link);                
            }
            return newList.Distinct<string>().ToList();
        }

        private static bool LinkWorks(string url)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(url);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}