﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Rss.Repository.Boundary.Model;

namespace Rss.Domain.Logic.Helpers
{
    public static class XmlConverter
    {
       public static ChannelEntity ConvertToChannel(string url)
        {
            ChannelEntity channel = new ChannelEntity();
            XmlDocument doc = new XmlDocument();
            channel.Items = ConvertToNews(url);
            if (channel.Items.Count > 0)
            {
                try
                {
                    doc.Load(url);

                    channel.XmlLink = url;
                    channel.Title = FormatTitle(doc.DocumentElement.SelectSingleNode("channel/title").InnerText);
                    channel.Description = doc.DocumentElement.SelectSingleNode("channel/description").InnerText;
                    channel.Link = doc.DocumentElement.SelectSingleNode("channel/link").InnerText;
                    channel.LastBuildDate = doc.DocumentElement.SelectSingleNode("channel/lastBuildDate").InnerText;
                    channel.Language = doc.DocumentElement.SelectSingleNode("channel/language").InnerText;

                }
                catch
                {

                }
                return channel;
            }
            else
            {
                return null;
            }
        }

        private static string FormatTitle(string title)
        {           
            var indexTvn24 = title.IndexOf("TVN24.pl -");

            if (indexTvn24 != -1)
                title = title.Substring(indexTvn24, title.Length - indexTvn24);

            var index1 = title.IndexOf("-");
            var index2 = title.LastIndexOf("-");

            if (index1 < 0)
                return title;

            if(index1!=index2)
                return title.Substring(index1+2, index2 - index1 -3);

            if (index1 + 2 >= title.Length)
                return title.Substring(0, index1 - 1);

            return title.Substring(index1+2, title.Length-index1-2);
        }
        
       public static List<ItemEntity> ConvertToNews(string url)
        {
            List<ItemEntity> news = new List<ItemEntity>();
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(url);
                XmlNodeList items = doc.DocumentElement.SelectNodes("channel/item");
                    foreach (XmlNode node in items)
                    {
                        news.Add(new ItemEntity());
                        news.Last().Title = node.FirstChild.InnerText;
                        node.RemoveChild(node.FirstChild);

                        news.Last().Link = node.FirstChild.InnerText;
                        node.RemoveChild(node.FirstChild);

                        //PROBA WYCIAGNIECIA ZDJECIA Z OPISU//

                        //string description = node.FirstChild.InnerText;
                        //node.RemoveChild(node.FirstChild);
                        //news.Last().ImageUrl = GetImgUrlFromDesc(description);

                        news.Last().Description = node.FirstChild.InnerText;
                        node.RemoveChild(node.FirstChild);

                        news.Last().PubDate = node.FirstChild.InnerText;
                        node.RemoveChild(node.FirstChild);
                    }
            }
            catch
            {
                //nie działą
            }
            return news;
        }

       public static string GetChannelDate(string url)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(url);
                return doc.DocumentElement.SelectSingleNode("channel/lastBuildDate").InnerText;
            }
            catch
            {
                return "http://ekstraklasa.tv/ekstraklasa-tv,83,m.xml";
            }

        }

        //private string GetImgUrlFromDesc(string desc)
        //{
        //    bool read = false;
        //    string img = "";
        //    foreach(char letter in desc)
        //    {
        //        if (read) img += letter;
        //        else if(letter == '\"' && !read) read = true;
        //        else if (letter == '\"' && read) read = false;
        //    }
        //    img.Remove(img.Last());
        //    return img;
        //}

        //private string DeleteImgTag(string desc)
        //{
        //    bool delete = false;
        //    string img = "";
        //    foreach (char letter in desc)
        //    {
        //        if (delete) img += letter;
        //        else if (letter == '\"' && !delete) delete = true;
        //        else if (letter == '\"' && delete) delete = false;
        //    }
        //    img.Remove(-1);
        //    return img;
        //}
    }
}