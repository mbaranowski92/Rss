﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rss.Domain.Logic.Helpers
{
    public static class StringListConverter
    {
        public static string ToString(IEnumerable<string> list)
        {
            var str = string.Join("/", list);
            return str;
        }

        public static List<string> ToList(string strList)
        {
            return strList.Split('/').ToList();
        }
    }
}
