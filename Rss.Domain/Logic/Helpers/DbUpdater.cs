﻿using System.Linq;
using Rss.Repository.Boundary;

namespace Rss.Domain.Logic.Helpers
{
    public class DbUpdater
    {
        private readonly IChannelRepository _rssRepo;

        public DbUpdater(IChannelRepository rssRepo)
        {
            _rssRepo = rssRepo;
        }

        public void CreateNewDb()
        {
            _rssRepo.CreateDb();

            var links = XmlLinkGetter.GetLinksFromUrl();
            foreach (var link in links)
            {
                var channel = XmlConverter.ConvertToChannel(link);
                if (channel != null)
                    _rssRepo.AddChannel(channel);
            }
        }

        public void UpdateDatabase()
        {
            var links = XmlLinkGetter.GetLinksFromUrl();
            var channelsInDb = _rssRepo.GetChannels().ToList();

            foreach (var link in links)
            {
                var channelInSite = XmlConverter.ConvertToChannel(link);
                if (channelInSite == null) continue;

                foreach (var channelInDb in channelsInDb)
                {
                    if (channelInSite.XmlLink != channelInDb.XmlLink) continue;

                    if (channelInSite.LastBuildDate != channelInDb.LastBuildDate)
                        _rssRepo.AddItemsToChannel(channelInSite);
                }
            }
        }

        public void ClearDoubledInDb()
        {
            var channelsCount = _rssRepo.ChannelsCount();
            for (var i = 1; i <= channelsCount; i++)
                ClearDoubledInChannel(i);
        }

        public void ClearDoubledInChannel(int id)
        {
            _rssRepo.ClearDoubledInChannel(id);
        }
    }
}